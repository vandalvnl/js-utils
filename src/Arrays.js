const toArray = (old) => [...old];

const filter = (array) => array.filter(Boolean);

const flat = (arr) => [].concat(...arr.map((v) => (Array.isArray(v) ? deepFlatten(v) : v)));

const reversePop = (arr, n = 1) => arr.slice(n);

const pop = (arr, n = 1) => arr.slice(-n);

const fill = (obj, length) => Array(length).fill(obj);

const findKey = (obj = {}, target) =>
	target in obj
		? obj[target]
		: Object.values(obj).reduce((acc, val) => {
				if (acc !== undefined) return acc;
				if (typeof val === "object") return dig(val, target);
		  }, undefined);

const keyAsValue = (obj) =>
	Object.keys(obj).reduce((acc, key) => {
		const val = obj[key];
		acc[val] = acc[val] || {};
		acc[val] = key;
		return acc;
	}, {});

const uniq = (arr) => [...new Set(myArray)];

const reverseForEach = (arr, callback) =>
	arr
		.slice(0)
		.reverse()
		.forEach(callback);

const repeats = (arr) => arr.filter((i) => arr.indexOf(i) === arr.lastIndexOf(i));

const repeatsBy = (arr, fn) => arr.filter((v, i) => arr.every((x, j) => (i === j) === fn(v, x, i, j)));

const intersection = (a, b) => {
	const s = new Set(b);
	return a.filter((x) => s.has(x));
};

export default {
	pop,
	fill,
	uniq,
	flat,
	filter,
	findKey,
	toArray,
	repeats,
	repeatsBy,
	reversePop,
	keyAsValue,
	intersection,
	reverseForEach,
};
