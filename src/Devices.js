const device = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
	? "mobile"
	: "desktop";

let geoLocation = {};

export default {
	device,
	innerWindow: {
		width: window.innerWidth,
		heigth: window.innerHeight,
	},
	outerWindow: {
		width: window.outerWidth,
		heigth: window.outerHeight,
	},
	languages: () => navigator.languages,
	browser: {
		name: navigator.appCodeName,
		version: navigator.appVersion,
	},
	platform = () => navigator.platform,
	userAgent = () => navigator.userAgent
};
