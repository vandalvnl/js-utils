const location = window.location;

const redirect = (url) => {
	const http = /^https?:\/\/.*/;
	if (http.test(url)) {
		location.href = url;
	}
	location.href = `https://${url}`;
};

export default {
	protocol: location.protocol,
	hostname: location.hostname,
	href: location.href,
	pathname: location.pathname,
	redirect,
};
