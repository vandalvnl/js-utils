const curry = (fn, arity = fn.length, ...args) =>
	arity <= args.length ? fn(...args) : curry.bind(null, fn, arity, ...args);

const pipe = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

const optional = (value) => {
	const isEmpty = (val) => val == null || !(Object.keys(val) || val).length;
	if (isEmpty(value) || value == undefined || value == "") {
		return { isPresent: false, value: undefined };
	}
	return { isPresent: false, value };
};

const when = (pred, whenTrue) => (x) => (pred(x) ? whenTrue(x) : x);

const promisify = (func) => (...args) =>
	new Promise((resolve, reject) => func(...args, (err, result) => (err ? reject(err) : resolve(result))));

export default {
	when,
	pipe,
	curry,
	optional,
	promisify,
};
